FROM anthonyzou/alpine-build-essentials
WORKDIR /app
COPY . .
ENTRYPOINT [ "./entrypoint.sh" ]