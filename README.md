# Introduction

This is a project (written in C++) done by me and my firends to improve our skills with software development in a team (there are three of us).

## Purpose of the project

1. Enhance our **coding skills** (focused on C++ in this case)
2. Learn and practice with **design patterns**
3. Getting more experience of **teamwork** (using gitlab for code collaboration)
4. See our capabilities of **working under pressure** (we decided to complete it in one week)

## Preparation

- We first came out with an UML diagram for the whole project
- Then we created a project plan and a work breakdown (What will be done by when) 

## Directory
- All the code is in `/src/`, the application (executable file `quadris`) can be compiled using `make` in `/src/` (Makefile is written)
- UML diagram is stored in `/UML/`

## How to run the application
If you have c++ already installed locally (ie., you can compile a normal c++ program), then you just need to go into `/src/` and run `make` and execute the executable file `quadris`, which is created from the `Makefile`.  

If you don't have c++ installed locally, you can consider using **docker**. There is a `Dockerfile` in the root directory, you can simply the following commands to build and run:  
```
docker build -t "quadris" .
docker run -it --rm --name="quadris" quadris
```

On the other hand, you can just run the shell `run_docker.sh`, which includes the two commands above.  

Note: if you are using *Windows*, you might need to add `winpty` in the front of `docker run` (2nd line of command).

The image created for this application is around 164 MB. By executing the above two line of commands, the image name and the container name for this application should both be `quadris`.

## UML diagram for this project
![avatar](/UML/UML.jpg)

---

# The Game - Quadris

Quadris is a game just like Tetris (but NOT real-time). A game of Quadris consists of a board, 11 columns in width and 15 rows in height.  Blocks consisting of four cells (tetrominoes) appear at the top of the screen, and you must drop them onto the board so as not to leave any gaps.  Once an entire row has been filled, it disappears, and the blocks above move down by one unit.

### Block
---
There are seven types of blocks (each block occupies two rows):

|I-block | J-block | L-block  |O-block| S-block |Z-block  |T-block  |  
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
||J&nbsp;&nbsp;|L|OO|&nbsp;&nbsp;&nbsp;SS|ZZ&nbsp;&nbsp;&nbsp;|TTT|
|IIII|JJJ|LLL|OO|SS&nbsp;&nbsp;|&nbsp;&nbsp;ZZ|T|

### Commands
---
- **left**  Moves the current block one cell to the left. No effect if not possible.
- **right**  As above, but to the right. 
- **down**  As above, but one cell downward. 
- **clockwise**  Rotates the block 90 degrees clockwise, as described earlier. No effect if not possible.
- **counterclockwise**  As above, but counterclockwise. 
- **drop**  Drop the current block.  It is (in one step) moved downward as far as possible until it comes into contact with either the bottom of the board or a block.  This command also triggers the next block to appear.  Even if a block is already as far down as it can go (as a result of executing the down command), it still needs to be dropped in order to get the next block. 
- **levelup**  Increases the difficulty level of the game by one.  (The block showing as next still comes next, but the subsequent blocks are generated using the new level).  If there is no higher level, this command has no effect.
- **leveldown**  As above, but decreases the difficulty level of the game by one.
- **norandom file**  Only used in levels 3 and 4, this command makes these levels non-random, instead taking input from the sequence file, starting from the beginning.  (This is to facilitate testing).
- **random**  Only used in levels 3 and 4, this command is opposite to the above one. 
- **sequence** file Executes the sequence of commands found in file. (This is to facilitate the construction of test cases). 
- **I, J, L, S, Z, O, T**  Useful during testing.  These commands replace the current undropped block with the designated block.  Heaviness is determined by the level number.  Note that, for **"heavy"** blocks, these commands do not cause a downward move. 
- **restart**  Clears the board and starts a new game 
- **hint** (Not implemented yet)  Suggests a landing place for the current block.  The game should suggest the best place to put the current block.  The hint is indicated on the text display by a block made of ?’s in the suggested position, and in the graphics display using the colour black.


### Levels
---
##### Test level
- **Level 0**: Takes its blocks in sequence from the file sequence.txt, or from another file, whose name is supplied on the command line.  This level is non-random, and can be used to test with a predetermined set of blocks.
##### Normal levels
- **Level 1**: S and Z blocks are less likely to drop (P=1/12). The other blocks (P=1/6). 
- **Level 2**: All blocks are selected with equal probability 
##### Hard levels
- **Level 3**: The probability for choosing any block is random (i.e., no fixed probability for choosing). Moreover, blocks generated in level 3 are **"heavy"**: every command to move or rotate the block will be followed immediately and automatically by a downward move of one row (if possible). 
- **Level 4**: In addition to the rules of Level 3, in Level 4 there is an external constructive force: every time you place 5 (and also 10, 15, etc.) blocks without clearing at least one row, a 1x1 block (indicated by * in text, and by the colour brown in graphics) is dropped onto your game board in the centre column. Once dropped, it acts like any other block; if it completes a row, the row disappears. So if you do not act quickly, these blocks will work to eventually split your screen into two, making the game difficult to play. 
 
### Some features to add convenience for the gameplay
---
- **Shortcuts**: All the commands (including new commands created by **Macro** command and commands being renamed) can be called by its distinguishable shortcuts. By distinguishable, it means this shortcut is unique among all the command names and shortcuts. Example: **lef** is enough to distinguish the **left** command from the **levelup** command, so the system should understand either **lef** or **left** as meaning **left**. 
- **Multiplier prefix**: All the commands related to block movement and some of the other commands can be called in the form of "xCOMMAND", where "x" is an integer and "COMMAND" is the command. By calling the command this way, the game logic will execute the corresponding command for "x" times. Note that, custom commands, which is created by **Macro** command can also be called with this fashion, but be careful with magic. Example, **3ri** means move to the right by three cells.
- **Rename command**: You can rename any existing command with any new names, which is not an existing command. Note that, The command "rename" can also be renamed. (Friendly reminder: if you renamed "showcmd" with an indescribable name and you forgot it, just type "help", which will tell you the current command name for "showcmd". From "showcmd", you can see all the current names for all the available commands.)
- **Macro command**: This allows you to create a new command, which is essentially a list of other commands (includes other commands created by **Macro** commands).
- **Show command**: Calling "showcmd" (any shortcuts for it, or the new name it is renamed to) will show all the commands that are available and their descriptions. 
- **Help command**: Since "showcmd" can be renamed to something else, an unchangeable (cannot be renamed) command is needed. I.e., "help", which can be called to see the current command name for "showcmd".

### Tools used in this project
---
- **Cesign pattern**: In general, the application is implemented with MVC design pattern. Observer design pattern is used between View (Display) and Model (GamePlay), and a controller used a strategy pattern. 
- **STL Smart pointers**: Memory (pointer) is handled by different kinds of smart pointers and guaranteed zero memory leak.
- **STL containers**: Vector, set, and their corresponding STL iterators are used to make the application more efficient and also easier to implement (code).

### Todo
---
- **hint** command is not implemented yet
- graphic UI is not implemented
