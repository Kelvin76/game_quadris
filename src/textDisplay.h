// textDisplay.h
// Text class.  Is responsible for text interface

#ifndef TEXT_DISPLAY_H
#define TEXT_DISPLAY_H

#include "display.h"

class TextDisplay : public Display {
public:
    TextDisplay(std::shared_ptr<Controller>, std::shared_ptr<GamePlay>);
    virtual ~TextDisplay();

    // Displays the board
    void display() override;
    // Run/start the view, used by the main function
    void run() override;

};  // TextDisplay

#endif