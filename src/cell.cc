#include <iostream>
#include "cell.h"

using namespace std;

Cell::Cell(char c) : blockType_(c), block_(nullptr) {};

Cell::~Cell() {};

char Cell::getChar() const {
    return blockType_;
}

shared_ptr<Block> Cell::getBlock() const {
    return block_;
}

void Cell::setChar(const char c) {
    blockType_ = c;
}

void Cell::setBlock(const shared_ptr<Block> b) {
    block_ = b;
}