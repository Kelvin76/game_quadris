// commandAnalyser.h
// CommandAnalyser class.  Is responsible for parsing and filtering the valid
// commands

#ifndef COMMANDANALYSER_H
#define COMMANDANALYSER_H

#include <string>
#include <unordered_map>
#include <utility>
#include "commandenum.h"

typedef std::pair<cmdEnumVec, size_t> cmdListPair;

class CommandAnalyser {
 public:
  CommandAnalyser();
  ~CommandAnalyser();

  // Returns true if the input is a valid command
  bool isCmdCommandValid(std::string);

  // First return value is true if the input is a valid
  // command with x parameters
  // Second return value is the list of cmd/params parsed from the input
  std::pair<bool, std::vector<std::string>> isCmdCommandValidWithXParameters(
      std::string, int = 0);

  // Returns true if the command is an internal command
  // Also operates the internal command inside this function
  bool isInternalCommand(std::string);

  // Returns true if the string input contains the enum input (command)
  bool isCmdCommand(std::string, COMMANDENUM) const;

  // Getter for the vector of commandEnum's
  cmdListPair getCommands(std::string);

  // Print the basic command info
  void printBasicCommandInfo() const;

 private:
  // initialize corresponding member variables
  void initializeCommandsMap_();
  // initialize/refresh shortcuts map
  void regenerateNameShortcutMap_();
  // "rename" the existing command
  bool changeCommandName_(std::string, std::string);
  // Add a command name for a sequence of commands ("macro" command)
  bool addNewCommand_(std::string, cmdEnumVec);
  // update the map due to the operation chose
  bool updateMaps_(char operation, std::string = "", std::string = "",
                   cmdEnumVec = cmdEnumVec{});
  // returns true if command exists (valid)
  bool commandExists_(std::string cmd) const;
  // print all valid commands
  void printValidCmds_(std::string = "") const;

  // get the string for the description of the command
  std::string getCommandDescription_(std::string) const;

  std::unordered_map<std::string, cmdEnumVec> commandsMap_;
  std::unordered_map<std::string, std::string> nameShortcutMap_;

};  // CommandAnalyser

#endif