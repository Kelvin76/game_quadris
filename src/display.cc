// Implementing Display.h
// Display class.  Is responsible for both graphic and text interface

#include <iostream>
#include "display.h"

using namespace std;

// Display constructor set the protected variables for model and controller
Display::Display(shared_ptr<Controller> c, shared_ptr<GamePlay> m)
    : Observer(m),
      model_(m),
      controller(c),
      cmdAnalyser(make_shared<CommandAnalyser>()),
      nextBlock(model_->getNextBlock()),
      board(model_->getBoard()),
      level(controller->getLevelType()),
      score(model_->getScore()),
      hiScore(model_->getHiScore()),
      gameInPlay(true) {}

Display::~Display() {}

// update the fields
void Display::update() {
  level = controller->getLevelType();
  score = model_->getScore();
  hiScore = model_->getHiScore();
  nextBlock = model_->getNextBlock();
  board = model_->getBoard();
  gameInPlay = model_->getGameStatus();

  // then display
  display();
}
