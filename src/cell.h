// cell.h
// Cell class. Is responsible for a functional cell class

#ifndef CELL_H
#define CELL_H

#include <iostream>
#include "block.h"

class Block;

class Cell {
public:
    Cell(char);
    ~Cell();

    // Accessors
    char getChar() const;
    std::shared_ptr<Block> getBlock() const;

    // Setters
    void setChar(const char c);
    void setBlock(const std::shared_ptr<Block>);

private:
    char blockType_;
    std::shared_ptr<Block> block_;
};  // Cell

#endif