/// Implementing `controller.h`

#include <assert.h>
#include <memory>
#include <utility>
#include <fstream>
#include "controller.h"

using namespace std;

// Mapping between BLOCKENUM and char
std::unordered_map<char, BLOCKENUM> blockChar2Enum = {
    {'I', IBLOCK}, {'T', TBLOCK}, {'Z', ZBLOCK}, {'S', SBLOCK},
    {'O', OBLOCK}, {'L', LBLOCK}, {'J', JBLOCK}, {'1', ONEBLOCK}};

Controller::Controller(std::shared_ptr<GamePlay> m, size_t startLevel,
                       size_t randomSeed, string blockFile)
    : model_(m),
      levelType_(startLevel),
      seed_(randomSeed),
      blockFile_(blockFile) {
  // set level type
  switch (startLevel) {
    case 0:
      level_ = make_unique<Level0>(blockFile_, blockChar2Enum);
      break;
    case 1:
      level_ = make_unique<Level1>();
      break;
    case 2:
      level_ = make_unique<Level2>();
      break;
    case 3:
      level_ = make_unique<Level3>();
      break;
    case 4:
      level_ = make_unique<Level4>();
      break;
    default:
      // impossible to reach here
      assert(0);
      break;
  }

  replaceBlockType_(getNextBlockType_());
  setNextBlockType_(getNextBlockType_());
}

Controller::~Controller() {}

// call the corresponding command
void Controller::commandInterpreter(shared_ptr<CommandAnalyser> cmdAnalyser,
                                    string input) {
  // input was already checked to be valid

  // get the command list and the value for multiplier
  auto inputPair = cmdAnalyser->getCommands(input);

  cmdEnumVec cmdList = inputPair.first;
  int multiplier = inputPair.second;

  // get parameter
  // some commands need a parameter for filename
  string filename = "";

  // special commands: norandom and sequence need one parameter
  if (cmdAnalyser->isCmdCommand(input, NORANDOMFILE) ||
      cmdAnalyser->isCmdCommand(input, SEQUENCEFILE)) {
    // if the command should have exactly 1 more parameter
    // but there are more commands, then abort
    auto validityAndParamList =
        cmdAnalyser->isCmdCommandValidWithXParameters(input, 1);

    // input is not valid, print info and return
    if (!validityAndParamList.first) {
      cout << "[Controller]: " << validityAndParamList.second.at(0)
           << " command should have exactly 1 parameter." << endl;
      return;
    }

    filename = validityAndParamList.second.at(1);
  }

  // if multiplier is not zero, than set the noNotifyCounter
  if (multiplier != 0) {
    if (cmdAnalyser->isCmdCommand(input, DOWN)) {
      level_->setNoNotify(model_, multiplier - 1, true);
    } else {
      level_->setNoNotify(model_, multiplier - 1, false);
    }
  }

  // call the corresponding function for all input commands
  for (auto cmd : cmdList) {
    switch (cmd) {
      case LEFT:
        level_->left(model_);
        break;
      case RIGHT:
        level_->right(model_);
        break;
      case DOWN:
        level_->down(model_);
        break;
      case CLOCKWISE:
        level_->clockwise(model_);
        break;
      case COUNTERCLOCKWISE:
        level_->counterClockwise(model_);
        break;
      case DROP:
        level_->drop(model_);
        setNextBlockType_(getNextBlockType_());
        break;
      case RESTART:
        level_->isRandom_ = true;
        level_->restart(model_);
        setNextBlockType_(getNextBlockType_());
        break;
      case HINT:
        level_->hint(model_);
        break;
      case LEVELUP:
        levelup_();
        break;
      case LEVELDOWN:
        leveldown_();
        break;
      case NORANDOMFILE:
        noRandom_(cmdAnalyser, filename);
        break;
      case RANDOM:
        random_();
        break;
      case SEQUENCEFILE:
        executeSeqFile_(cmdAnalyser, filename);
        break;
      case ICOMMAND:
        replaceBlockType_(IBLOCK);
        break;
      case TCOMMAND:
        replaceBlockType_(TBLOCK);
        break;
      case ZCOMMAND:
        replaceBlockType_(ZBLOCK);
        break;
      case SCOMMAND:
        replaceBlockType_(SBLOCK);
        break;
      case OCOMMAND:
        replaceBlockType_(OBLOCK);
        break;
      case LCOMMAND:
        replaceBlockType_(LBLOCK);
        break;
      case JCOMMAND:
        replaceBlockType_(JBLOCK);
        break;
      default:
        // impossible to reach here
        assert(0);
        break;
    }
  }
}

size_t Controller::getLevelType() { return levelType_; }

void Controller::random_() {
  if (levelType_ == 3 || levelType_ == 4) {
    level_->isRandom_ = true;
    cout << "Blocks are now random. Changes take effect after indicated block."
         << endl;
  } else {
    cout << "This command can only be called in Level 3 and 4" << endl;
  }
}

void Controller::noRandom_(shared_ptr<CommandAnalyser> cmdAnalyser,
                           string filename) {
  // get the filename, which is the exactly first parameter aftert the command

  if (levelType_ == 3 || levelType_ == 4) {
    level_->isRandom_ = false;
    char input = (char)0;
    ifstream file;
    file.open(filename);

    // file does not exist
    if (!file) {
      cout << "[Error]: File indicated does not exist, please enter again."
           << endl;
      return;
    }

    // create the rest of the vec
    blockEnumVec bv{};
    level_->fileVecPtr_ = 0;
    while (file >> input) {
      bv.push_back(blockChar2Enum[input]);
    }
    level_->lvlZeroFileVec_ = bv;
    cout << "The blocks are now being read from file. Changes take effect "
            "after indicated next block."
         << endl;
  } else {
    cout << "This command can only be called in Level 3 and 4" << endl;
  }
}

void Controller::levelup_() {
  if (levelType_ < MAXLEVEL) {
    levelType_++;
    unique_ptr<Level> newLevel;
    switch (levelType_) {
      case 1:
        level_ = move(make_unique<Level1>());
        break;
      case 2:
        level_ = move(make_unique<Level2>());
        break;
      case 3:
        level_ = move(make_unique<Level3>());
        break;
      case 4:
        level_ = move(make_unique<Level4>());
        break;
      default:
        // impossible to reach here
        assert(0);
        break;
    }
  }
  model_->levelchange();
}

void Controller::leveldown_() {
  if (levelType_ > 0) {
    levelType_--;
    switch (levelType_) {
      case 0:
        level_ = move(make_unique<Level0>(blockFile_, blockChar2Enum));
        break;
      case 1:
        level_ = move(make_unique<Level1>());
        break;
      case 2:
        level_ = move(make_unique<Level2>());
        break;
      case 3:
        level_ = move(make_unique<Level3>());
        break;
      default:
        // impossible to reach here
        assert(0);
        break;
    }
  }
  model_->levelchange();
}

BLOCKENUM Controller::getNextBlockType_() {
  return level_->getNextBlock(model_, seed_);
}

void Controller::executeSeqFile_(shared_ptr<CommandAnalyser> cmdAnalyser,
                                 string filename) {
  string input = "";

  ifstream file;
  file.open(filename);

  // file does not exist
  if (!file) {
    cout << "[Error]: File indicated does not exist, please enter again."
         << endl;
    return;
  }

  if (file.good()) {
    while (file >> input) {
      commandInterpreter(cmdAnalyser, input);
    }
  }

  // reset the file
  file.close();
}

void Controller::replaceBlockType_(BLOCKENUM be) { model_->setCurBlock(be); }

void Controller::setNextBlockType_(BLOCKENUM be) { model_->setNextBlock(be); }