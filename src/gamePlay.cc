// Implementing GamePlay.h
// GamePlay class.  Is responsible for game logic
// It knows nothing about views or controllers.

#include <memory>
#include <cmath>
#include "gamePlay.h"

using namespace std;

GamePlay::GamePlay(int levelType)
    : board_(make_shared<Board>()),
      score_(0),
      hiScore_(0),
      levelType_(levelType),
      noNotifyCounter_(0),
      gameInPlay_(true){};

GamePlay::~GamePlay() {}

bool GamePlay::left() {
  coordinates coords = curBlock_->getCoordinates();
  coordinates possiblecoords = curBlock_->getLeftCoordinates();

  bool movePossible = board_->move(possiblecoords, coords);

  if (movePossible) {
    curBlock_->left();
  }

  if (noNotifyCounter_ == 0) {
    notify();
  } else {
    noNotifyCounter_--;
  }
  return movePossible;
}

bool GamePlay::right() {
  coordinates coords = curBlock_->getCoordinates();
  coordinates possiblecoords = curBlock_->getRightCoordinates();

  bool movePossible = board_->move(possiblecoords, coords);

  if (movePossible) {
    curBlock_->right();
  }

  if (noNotifyCounter_ == 0) {
    notify();
  } else {
    noNotifyCounter_--;
  }
  return movePossible;
}

bool GamePlay::down() {
  coordinates coords = curBlock_->getCoordinates();
  coordinates possiblecoords = curBlock_->getDownCoordinates();

  bool movePossible = board_->move(possiblecoords, coords);

  if (movePossible) {
    curBlock_->down();
  }

  if (noNotifyCounter_ == 0) {
    notify();
  } else {
    cout << "Notify:" << noNotifyCounter_ << endl;
    noNotifyCounter_--;
  }
  return movePossible;
}

bool GamePlay::clockwise() {
  coordinates coords = curBlock_->getCoordinates();
  coordinates possiblecoords = curBlock_->getClockwiseCoordinates();

  bool movePossible = board_->move(possiblecoords, coords);

  if (movePossible) {
    curBlock_->clockwise();
  }

  if (noNotifyCounter_ == 0) {
    notify();
  } else {
    noNotifyCounter_--;
  }
  return movePossible;
}

bool GamePlay::counterClockwise() {
  coordinates coords = curBlock_->getCoordinates();
  coordinates possiblecoords = curBlock_->getCounterClockwiseCoordinates();

  bool movePossible = board_->move(possiblecoords, coords);

  if (movePossible) {
    curBlock_->counterClockwise();
  }

  if (noNotifyCounter_ == 0) {
    notify();
  } else {
    noNotifyCounter_--;
  }
  return movePossible;
}

bool GamePlay::drop() {
  coordinates coords = curBlock_->getCoordinates();
  coordinates possiblecoords = curBlock_->getDownCoordinates();
  bool movePossible = board_->move(possiblecoords, coords);

  noNotifyCounter_ = 0;

  while (movePossible) {
    curBlock_->down();

    coords = curBlock_->getCoordinates();
    possiblecoords = curBlock_->getDownCoordinates();
    movePossible = board_->move(possiblecoords, coords);
  }

  // call updateScore (clears lines and updates score appropriately)
  updateScore();

  curBlock_ = generateBlock(nextBlock_, false);
  return true;
}

void GamePlay::hint() {
  // TODO: Implementation
  cout << "Not implemented." << endl;
}

void GamePlay::restart() {
  board_->reset();
  score_ = 0;
  curBlock_ = generateBlock(nextBlock_);
}

void GamePlay::levelchange() {
  if (noNotifyCounter_ == 0) {
    notify();
  } else {
    noNotifyCounter_--;
  }
}

shared_ptr<Block> GamePlay::generateBlock(BLOCKENUM be, bool override) {
  // each block now carries info on the level where they were made
  shared_ptr<Block> b = make_shared<Block>(be, levelType_);

  bool possible;
  if (curBlock_ == NULL)  // first time
  {
    possible = board_->newBlock(b, false, {});
  } else {
    coordinates coords = curBlock_->getCoordinates();
    possible = board_->newBlock(b, override, coords);
  }

  if (!possible) {
    gameInPlay_ = false;
    notify();
    gameInPlay_ = true;
    restart();
  }

  return possible ? b : curBlock_;
}

void GamePlay::generateOneBlock() {
  bool possible = board_->addOneBlock();
  if (!possible) {
    gameInPlay_ = false;
    notify();
    gameInPlay_ = true;
    restart();
  }
}

void GamePlay::updateScore() {
  // check all rows to see if they're filled
  bool rowComplete;
  vector<vector<Cell>> grid = board_->getGrid();
  size_t rowNum = board_->getRowNum();
  size_t colNum = board_->getColNum();

  // vector containing the row idx positions of completed lines
  vector<int> completedLines;

  // find indexes of completed rows
  // top three save rows don't house any blocks so we start "i" at 3
  for (size_t i = 3; i < rowNum; i++) {
    rowComplete = true;
    for (size_t j = 0; j < colNum; j++) {
      // set rowComplete flag to false if one block in the row is blank
      if (grid.at(i).at(j).getChar() == ' ') {
        rowComplete = false;
      }
    }

    // if the flag never gets set to false, the row is complete
    if (rowComplete) {
      completedLines.push_back(i);
    }
  }

  // no changes are made if no lines are empty
  if (!completedLines.empty()) {
    // score += current level + # of completed lines after a single
    // "down" command
    score_ += pow(levelType_ + completedLines.size(), 2);

    // add bonus points when an entire block is taken
    // remove elements from grid
    for (size_t i = 0; i < completedLines.size(); i++) {
      for (size_t j = 0; j < colNum; j++) {
        // set char to be empty
        grid.at(completedLines[i]).at(j).setChar(' ');

        // check # of remaining squares on block
        shared_ptr<Block> b = grid.at(completedLines[i]).at(j).getBlock();
        int remainingBlocks = b->getRemainingBlocks() - 1;
        b->setRemainingBlocks(remainingBlocks);

        // if the remaining # of squares on a block hits 0,
        //   add bonus points
        if (remainingBlocks == 0) {
          score_ += pow(b->getLevelOfOrigin() + 1, 2);
        }
      }
    }

    // descrbes the # rows we should move the line down
    int moveDownCounter = 0;
    // move rows of blocks down to fill the gaps
    for (int i = rowNum - 1; i >= 3; i--) {
      if (i == completedLines.back()) {
        // every time an empty row is detected, move rows above
        //   it +1 space down
        completedLines.pop_back();
        moveDownCounter += 1;
      } else if (i > completedLines.back()) {
        // move each square of the row down "moveDownCounter" # of times
        for (size_t j = 0; j < colNum; j++) {
          // extract info from square we're going to move down
          char gridChar = grid.at(i).at(j).getChar();
          shared_ptr<Block> gridBlock = grid.at(i).at(j).getBlock();

          // move the info "moveDownCounter" # of square down
          grid.at(i + moveDownCounter).at(j).setChar(gridChar);
          grid.at(i + moveDownCounter).at(j).setBlock(gridBlock);

          // empty the rows that were just moved from
          if (moveDownCounter != 0) {
            grid.at(i).at(j).setChar(' ');
            grid.at(i).at(j).setBlock(nullptr);
          }
        }
      }
    }

    // update high score if score gets higher than the prev high score
    if (score_ > hiScore_) {
      hiScore_ = score_;
    }

    // finally sets the board grid to the new grid
    board_->setGrid(grid);
  }
}

// Accessors
int GamePlay::getScore() const { return score_; }

int GamePlay::getHiScore() const { return hiScore_; }

shared_ptr<Board> GamePlay::getBoard() const { return board_; }

BLOCKENUM GamePlay::getNextBlock() const { return nextBlock_; }

bool GamePlay::getGameStatus() const { return gameInPlay_; }

// Mutators
void GamePlay::setCurBlock(BLOCKENUM b) {
  curBlock_ = generateBlock(b);
  notify();
}

void GamePlay::setNextBlock(BLOCKENUM b) {
  nextBlock_ = b;
  notify();
}

void GamePlay::setNoNotify(int n) { noNotifyCounter_ = n; }