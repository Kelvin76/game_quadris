// GamePlay.h
// GamePlay class.  Is responsible for game logic
// It knows nothing about views or controllers.

#ifndef MVC_GamePlay_H
#define MVC_GamePlay_H

#include <iostream>
#include <memory>
#include "board.h"
#include "subject.h"

class Block;
class Board;

class GamePlay : public Subject {
public:
    GamePlay(int);
    ~GamePlay();

    // Game Commands
    // Moves the current block one cell to the left
    bool left();
    // Moves the current block one cell to the right
    bool right();
    // Moves the current block one cell downward
    bool down();
    // Rotates the current block 90 degrees clockwise
    bool clockwise();
    // Rotates the current block 90 degrees counterclockwise
    bool counterClockwise();
    // Drop the current block
    bool drop();
    // Suggests a landing place for the current block
    void hint();
    // Clears the board & current score
    void restart();
    // Notifies observer to update
    void levelchange();
    // Generates a new block based on the type and places it on the board
    std::shared_ptr<Block> generateBlock(BLOCKENUM, bool = true);
    // Generates a * block in the centre of the board (Level 4)
    void generateOneBlock();
    void updateScore();

    // Getters
    int getScore() const;
    int getHiScore() const;
    std::shared_ptr<Board> getBoard() const;
    BLOCKENUM getNextBlock() const;
    bool getGameStatus() const;

    // Setters
    void setCurBlock(BLOCKENUM);
    void setNextBlock(BLOCKENUM);
    void setNoNotify(int);

private:
    std::shared_ptr<Board> board_;
    int score_;
    int hiScore_;
    int levelType_;
    size_t noNotifyCounter_;
    bool gameInPlay_;
    std::shared_ptr<Block> curBlock_;
    BLOCKENUM nextBlock_;
};  // GamePlay

#endif