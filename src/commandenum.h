// command.h
// COMMANDENUM. Is responsible for enumeration of all valid commands

#ifndef COMMANDENUM_H
#define COMMANDENUM_H

#include <set>
#include <vector>

// Note: do not change the order !!
// when adding new commands, please also modify (if needed)
// variable commandNames in `commandAnalyser.cc`and modify
// commandInterpreter() in controller.cc
enum COMMANDENUM {
  LEFT = 0,  // Basic commands
  RIGHT,
  DOWN,
  CLOCKWISE,
  COUNTERCLOCKWISE,
  DROP,
  RESTART,
  HINT,
  LEVELUP,
  LEVELDOWN,
  NORANDOMFILE,
  RANDOM,
  SEQUENCEFILE,
  ICOMMAND,
  TCOMMAND,
  ZCOMMAND,
  SCOMMAND,
  OCOMMAND,
  LCOMMAND,
  JCOMMAND,
  INTERNALCOMMANDS,  // Below are the internal commands
  RENAME,
  MACRO,
  SHOWCMD,
  HELP,
};

typedef std::set<COMMANDENUM> cmdEnumSet;
typedef std::vector<COMMANDENUM> cmdEnumVec;

#endif