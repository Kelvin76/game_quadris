// graphicalDisplay.h
// Graphical class.  Is responsible for graphical interface

#ifndef GRAPHICAL_DISPLAY_H
#define GRAPHICAL_DISPLAY_H

#include "display.h"

class GraphicalDisplay : public Display {
public:
    GraphicalDisplay(std::shared_ptr<Controller>, std::shared_ptr<GamePlay>);
    virtual ~GraphicalDisplay();

    // Displays the board
    void display() override;
    // Run/start the view, used by the main function
    void run() override;

};  // GraphicalDisplay

#endif