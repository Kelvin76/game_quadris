// trie.h
// Trie class. A trie class for lower-case english characters.

#ifndef TRIE_H
#define TRIE_H

#include <memory>
#include <string>
#include <vector>

#define totalNumOfChar 26

class Trie {
 public:
  Trie();

  // Inserts a word into the trie.
  void insert(std::string);
  // Returns if the word is in the trie.
  bool search(std::string);
  // Returns if there is any word in the trie that starts with the given
  bool startsWith(std::string);
  // Returns if there are multiple words in the trie that starts with the given
  bool multipleStartsWith(std::string);
  // return the shorest unambiguous prefix of the input
  std::string findShortcut(std::string);
  // return all the shorest unambiguous prefix of the input
  std::vector<std::string> findAllShortcut(std::string);

 private:
  // Return True if the node has two children
  bool hasTwoChildren_();

  // Children
  std::shared_ptr<Trie> Next[totalNumOfChar];
  // True if leaf child
  bool isEnd;
};  // Trie

#endif