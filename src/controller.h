// controller.h
// Controller class. Is responsible for translating UI events (from the Display)
// into method calls to the Model.

#ifndef MVC_CONTROLLER_H
#define MVC_CONTROLLER_H

// later use
// #include <gtkmm.h>

#include <memory>
#include <utility>
#include "level.h"
#include "gamePlay.h"
#include "commandAnalyser.h"

class GamePlay;

class Controller {
 public:
  Controller(std::shared_ptr<GamePlay>, size_t startLevel, size_t randomSeed, std::string blockFile);
  ~Controller();

  // Call the corresponding command
  void commandInterpreter(std::shared_ptr<CommandAnalyser>, std::string);
  // Return the current level (number)
  size_t getLevelType();

 private:
  // Restores randomness in levels 3 & 4
  void random_();
  // Makes the level nonrandom and takes input from sequence file
  void noRandom_(std::shared_ptr<CommandAnalyser>, std::string);
  // Increases the difficulty level of the game by one
  void levelup_();
  // Decreases the difficulty level of the game by one.
  void leveldown_();
  // Return the block type of the next block
  BLOCKENUM getNextBlockType_();
  // TODO: Come up with a method definition
  void executeSeqFile_(std::shared_ptr<CommandAnalyser>, std::string);
  // Replaces the current block with the specified block
  void replaceBlockType_(BLOCKENUM);
  // Sets the next block as the specified block
  void setNextBlockType_(BLOCKENUM);

  // Main game logic (follows MVC pattern)
  std::shared_ptr<GamePlay> model_;
  // Stores current level type (e.g. 0,1,..)
  size_t levelType_;
  // Level class (follows strategy pattern)
  std::unique_ptr<Level> level_;
  // seed value
  size_t seed_;
  // script file to read block from for level 0
  std::string blockFile_;

};  // Controller

#endif