// Implementing block.h
// block Class. Is responsible for creating an object for block

#include <iostream>
#include <vector>
#include "block.h"

using namespace std;

// mapping from block type to its rotation configurations {row offset, col
// offset}
// {row offset, col offset} can be added directly to anchor's {row, col} to get
// block coords
// THIS IS NOT {x position, y position}
std::map<BLOCKENUM, rotations> hardCodedRotations = {
    {IBLOCK,
     {{{0, 0}, {0, 1}, {0, 2}, {0, 3}},
      {{0, 0}, {-1, 0}, {-2, 0}, {-3, 0}},
      {{0, 0}, {0, 1}, {0, 2}, {0, 3}},
      {{0, 0}, {-1, 0}, {-2, 0}, {-3, 0}}}},
    {TBLOCK,
     {{{0, 1}, {-1, 0}, {-1, 1}, {-1, 2}},
      {{0, 1}, {-1, 1}, {-2, 1}, {-1, 0}},
      {{0, 0}, {0, 1}, {0, 2}, {-1, 1}},
      {{0, 0}, {-1, 0}, {-2, 0}, {-1, 1}}}},
    {ZBLOCK,
     {{{-1, 0}, {-1, 1}, {0, 1}, {0, 2}},
      {{0, 0}, {-1, 0}, {-1, 1}, {-2, 1}},
      {{-1, 0}, {-1, 1}, {0, 1}, {0, 2}},
      {{0, 0}, {-1, 0}, {-1, 1}, {-2, 1}}}},
    {SBLOCK,
     {{{0, 0}, {0, 1}, {-1, 1}, {-1, 2}},
      {{0, 1}, {-1, 1}, {-1, 0}, {-2, 0}},
      {{0, 0}, {0, 1}, {-1, 1}, {-1, 2}},
      {{0, 1}, {-1, 1}, {-1, 0}, {-2, 0}}}},
    {OBLOCK,
     {{{0, 0}, {0, 1}, {-1, 0}, {-1, 1}},
      {{0, 0}, {0, 1}, {-1, 0}, {-1, 1}},
      {{0, 0}, {0, 1}, {-1, 0}, {-1, 1}},
      {{0, 0}, {0, 1}, {-1, 0}, {-1, 1}}}},
    {LBLOCK,
     {{{0, 0}, {0, 1}, {0, 2}, {-1, 2}},
      {{0, 0}, {0, 1}, {-1, 0}, {-2, 0}},
      {{0, 0}, {-1, 0}, {-1, 1}, {-1, 2}},
      {{0, 1}, {-1, 1}, {-2, 1}, {-2, 0}}}},
    {JBLOCK,
     {{{0, 0}, {0, 1}, {0, 2}, {-1, 0}},
      {{0, 0}, {-1, 0}, {-2, 0}, {-2, 1}},
      {{0, 2}, {-1, 2}, {-1, 1}, {-1, 0}},
      {{0, 0}, {0, 1}, {-1, 1}, {-2, 1}}}},
    {ONEBLOCK, {{{0, 0}}}}};

Block::Block(BLOCKENUM blockType, int levelOfOrigin) {
  // set the block type
  blockType_ = blockType;
  // get the rotation configurations for the corresponding block type
  rotConfig_ = hardCodedRotations[blockType];
  // set the default rotation config the block starts at (always the first
  // element)
  relativeCoords_ = rotConfig_[0];
  rotConfigPosition_ = 0;

  if (blockType_ == BLOCKENUM::ONEBLOCK) {
    anchor_.push_back(3);
    anchor_.push_back(5);
  } else {
    // set the initial coordinates of the anchor (depends on how tall the
    // block is in its default rotation configuration)
    int blockHeight = 0;
    // find how tall block is by finding smallest row coordinate value
    for (auto coord : relativeCoords_) {
      if (coord[0] < blockHeight) {
        blockHeight = coord[0];
      }
    }

    // set anchor row
    anchor_.push_back(3 + (-blockHeight));
    // set anchor col
    anchor_.push_back(0);
  }

  // set block age (number of squares left in block)
  if (blockType == ONEBLOCK) {
    remainingBlocks_ = 1;
  } else {
    remainingBlocks_ = 4;
  }

  // set block to the level it was created at (set it to 0 for now)
  levelOfOrigin_ = levelOfOrigin;
}

Block::~Block() {}

// Accessors
coordinates Block::getCoordinates() const {
  coordinates currentCoords;
  // apply offset anchor to relative coords to get current coords
  for (auto coord : relativeCoords_) {
    vector<int> temp{anchor_[0] + coord[0], anchor_[1] + coord[1]};
    currentCoords.insert(temp);
  }

  return currentCoords;
}

BLOCKENUM Block::getType() const { return blockType_; }

int Block::getRemainingBlocks() const { return remainingBlocks_; }

int Block::getLevelOfOrigin() const { return levelOfOrigin_; }

// Accessors for post translation coordinates
coordinates Block::getLeftCoordinates() const {
  anchor newAnchor = anchor_;
  newAnchor.at(1) -= 1;
  return getTestCoords(newAnchor, relativeCoords_);
}

coordinates Block::getRightCoordinates() const {
  anchor newAnchor = anchor_;
  newAnchor.at(1) += 1;
  return getTestCoords(newAnchor, relativeCoords_);
}

coordinates Block::getDownCoordinates() const {
  anchor newAnchor = anchor_;
  newAnchor.at(0) += 1;
  return getTestCoords(newAnchor, relativeCoords_);
}

coordinates Block::getClockwiseCoordinates() const {
  int testRotConfigPos = rotConfigPosition_;
  if (size_t(testRotConfigPos) < rotConfig_.size() - 1) {
    testRotConfigPos += 1;
  } else {
    // goes back to 1st configuration
    testRotConfigPos = 0;
  }

  // change relative coords to next config
  coordinates newRelativeCoords = rotConfig_[testRotConfigPos];
  return getTestCoords(anchor_, newRelativeCoords);
}

coordinates Block::getCounterClockwiseCoordinates() const {
  int testRotConfigPos = rotConfigPosition_;
  if (testRotConfigPos > 0) {
    testRotConfigPos -= 1;
  } else {
    // goes back to 1st configuration
    testRotConfigPos = rotConfig_.size() - 1;
  }

  // change relative coords to next config
  coordinates newRelativeCoords = rotConfig_[testRotConfigPos];
  return getTestCoords(anchor_, newRelativeCoords);
}

// get the string to print the block
// but it adds the input before each line
string Block::getStr4Block(string str) const {
  string result = "";
  // first one goes negative (height)
  int blockHeight = 0;
  // second one goes positive (width)
  int blockWidth = 0;
  // find height & width of the block
  for (auto coord : relativeCoords_) {
    if (coord.at(0) < blockHeight) {
      blockHeight = coord.at(0);
    }
    if (coord.at(1) > blockWidth) {
      blockWidth = coord.at(1);
    }
  }

  // set up the string for returning
  for (int i = blockHeight; i <= 0; i++) {
    // add the header
    result += str;

    // string for each row
    for (int j = 0; j <= blockWidth; j++) {
      if (relativeCoords_.find({i, j}) == relativeCoords_.end()) {
        result += " ";
      } else {
        result += (char)blockType_;
      }
    }
    if (i != blockHeight - 1) {
      result += "\n";
    }
  }
  return result;
}

// Translation and Rotation Commands
void Block::left() { anchor_.at(1) -= 1; }

void Block::right() { anchor_.at(1) += 1; }

void Block::down() { anchor_.at(0) += 1; }

void Block::clockwise() {
  if (size_t(rotConfigPosition_) < rotConfig_.size() - 1) {
    rotConfigPosition_ += 1;
  } else {
    // goes back to 1st configuration
    rotConfigPosition_ = 0;
  }

  // change relative coords to next config
  relativeCoords_ = rotConfig_[rotConfigPosition_];
}

void Block::counterClockwise() {
  if (size_t(rotConfigPosition_) > 0) {
    rotConfigPosition_ -= 1;
  } else {
    // goes back to 1st configuration
    rotConfigPosition_ = rotConfig_.size() - 1;
  }

  // change relative coords to next config
  relativeCoords_ = rotConfig_[rotConfigPosition_];
}

// mutators
void Block::setRemainingBlocks(const int remainingBlocks) {
  remainingBlocks_ = remainingBlocks;
}

// Helper function for test methods
coordinates Block::getTestCoords(anchor newAnchor,
                                 coordinates newRelCoords) const {
  coordinates currentCoords;
  // apply offset anchor to relative coords to get current coords
  for (auto coord : newRelCoords) {
    vector<int> temp{newAnchor[0] + coord[0], newAnchor[1] + coord[1]};
    currentCoords.insert(temp);
  }

  return currentCoords;
}

ostream &operator<<(std::ostream &sout, Block &b) {
  // first one goes negative (height)
  int blockHeight = 0;
  // second one goes positive (width)
  int blockWidth = 0;
  // find height & width of the block
  for (auto coord : b.relativeCoords_) {
    if (coord.at(0) < blockHeight) {
      blockHeight = coord.at(0);
    }
    if (coord.at(1) > blockWidth) {
      blockWidth = coord.at(1);
    }
  }

  // print block
  for (int i = blockHeight; i <= 0; i++) {
    for (int j = 0; j <= blockWidth; j++) {
      if (b.relativeCoords_.find({i, j}) == b.relativeCoords_.end()) {
        sout << ' ';
      } else {
        sout << (char)b.blockType_;
      }
    }
    if (i != blockHeight - 1) sout << endl;
  }
  return sout;
}