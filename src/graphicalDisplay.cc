// Implementing graphicalDisplay.h
// Graphical class.  Is responsible for graphical interface

#include "graphicalDisplay.h"

using namespace std;

GraphicalDisplay::GraphicalDisplay(std::shared_ptr<Controller> c,
                                   std::shared_ptr<GamePlay> m)
    : Display(c, m) {
  // Register TextDisplay as observer of gamePlay
  subject_->subscribe(this);
}

GraphicalDisplay::~GraphicalDisplay() {
  subject_->unsubscribe(this);
}

void GraphicalDisplay::display() {
  // some code
}

void GraphicalDisplay::run() {}
