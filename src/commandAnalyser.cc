// Implementing commandAnalyser.h
// Command class.  Is responsible for parsing and filtering the valid commands

#include <algorithm>
#include <iostream>
#include <locale>
#include <sstream>
#include <vector>
#include "commandAnalyser.h"
#include "trie.h"

using namespace std;

// All the original commands that are valid
// do not change the order !!
// when adding new commands, please also modify variable
// COMMANDENUM in `commandenum.h` and modify (if needed)
// commandInterpreter() in controller.cc
vector<string> basicCommandNames = {
    "left",     "right",   "down",     "clockwise", "counterclockwise",
    "drop",     "restart", "hint",     "levelup",   "leveldown",
    "norandom", "random",  "sequence", "I",         "T",
    "Z",        "S",       "O",        "L",         "J",
};

vector<string> internalCommandNames = {
    "rename", "macro", "showcmd", "help",
};

static size_t const indexForNoran = 10;
static size_t const indexForSeq = 12;
static size_t const indexForRename = 0;
static size_t const indexForMacro = 1;
static size_t const indexForShow = 2;

vector<string> customCommandNames = {};

CommandAnalyser::CommandAnalyser() {
  // initialize the maps
  initializeCommandsMap_();
  regenerateNameShortcutMap_();
}

CommandAnalyser::~CommandAnalyser() {}

// initialize the commands map
void CommandAnalyser::initializeCommandsMap_() {
  // initialize the map

  // add basic command names
  for (size_t ind = 0; ind < basicCommandNames.size(); ++ind) {
    // in the form of < commandName, vector of COMMANDENUM >
    commandsMap_[basicCommandNames.at(ind)] = cmdEnumVec{COMMANDENUM(ind)};
  }

  // add internal command names
  for (size_t ind = 0; ind < internalCommandNames.size(); ++ind) {
    size_t enumIndex = INTERNALCOMMANDS + 1 + ind;
    // in the form of < commandName, vector of COMMANDENUM >
    commandsMap_[internalCommandNames.at(ind)] =
        cmdEnumVec{COMMANDENUM(enumIndex)};
  }
}

// update the commands map
// First parameter is a distinguisher:
//    'a': add command
//    'c': change command name from cmdName to newName
//    'd': delete command (Not needed right now)
bool CommandAnalyser::updateMaps_(char operation, string cmdName,
                                  string newName, cmdEnumVec cmds) {
  bool success = false;

  // find the entry with cmdName in commandsMap_ (for case 'c')
  auto it = commandsMap_.find(cmdName);
  switch (operation) {
    case 'a':
      // make sure the new name does not exists
      if (commandExists_(newName)) {
        // print error message
        cout << "[cmdAnalyser]: " << newName + ": command already exists."
             << endl;
        printBasicCommandInfo();
        return success;
      }

      // add the command into customCommands
      customCommandNames.push_back(newName);

      // add the command in the form of <commandName,vectorofCOMMANDENUM>
      commandsMap_[newName].insert(end(commandsMap_[newName]), begin(cmds),
                                   end(cmds));
      break;
    case 'c':
      // change the command name from cmdName to newName
      // i.e. change the key in the corresponding entry in commandsMap_

      // make sure the original name exists
      if (!commandExists_(cmdName)) {
        // print error message
        cout << "[cmdAnalyser]: "
             << cmdName + ": command to be renamed is not found." << endl;
        printBasicCommandInfo();
        return success;
      }

      // rename command to the same name
      if (cmdName == newName) {
        success = true;
        return success;
      }
      // make sure the new name does not exists
      else if (commandExists_(newName)) {
        // did find the newName
        // print the existing command names
        printValidCmds_("[cmdAnalyser]: " + newName +
                        ": command to be renamed to already exists. "
                        " The existing command names are:");
        return success;
      }

      // change the command names in commandsMap_
      // add the new entry with newName in commandsMap_
      commandsMap_[newName] = (*it).second;
      // delete the old entry
      commandsMap_.erase(it);

      // modify the corresponding vector that the command is in
      {
        bool found = false;
        {
          auto it =
              find(begin(basicCommandNames), end(basicCommandNames), cmdName);
          if (it != basicCommandNames.end()) {
            (*it) = newName;
            found = true;
          }
        }
        if (!found) {
          auto it = find(begin(internalCommandNames), end(internalCommandNames),
                         cmdName);
          if (it != internalCommandNames.end()) {
            (*it) = newName;
            break;
          }
        }
        if (!found) {
          auto it =
              find(begin(customCommandNames), end(customCommandNames), cmdName);
          if (it != customCommandNames.end()) {
            (*it) = newName;
            break;
          }
        }
      }

      break;
    default:
      break;
  }
  // update the command shortcuts
  regenerateNameShortcutMap_();
  success = true;
  return success;
}

void CommandAnalyser::regenerateNameShortcutMap_() {
  // generate the shortcuts and map to the original
  // name for all the existing commands

  // clear the map
  if (!nameShortcutMap_.empty()) {
    nameShortcutMap_.clear();
  }

  vector<string> names = {};
  // get all command names
  for (auto it = commandsMap_.begin(); it != commandsMap_.end(); ++it) {
    names.push_back((*it).first);
  }

  // using a prefix trie
  shared_ptr<Trie> tr = make_shared<Trie>();

  // construct the trie
  for (auto word : names) {
    (*tr).insert(word);
  }

  // inserting all the <shortcut, commandName> into the map
  for (auto word : names) {
    auto shortcuts = (*tr).findAllShortcut(word);
    for (auto sc : shortcuts) {
      nameShortcutMap_[sc] = word;
    }
  }
  // Note: since we constructed the trie with the same set of strings,
  // there is no need to check if the string is in the trie
}

bool CommandAnalyser::isCmdCommandValid(string cmd) {
  // input is empty, not valid
  if (cmd.empty()) {
    return false;
  }

  // if there exists number in the begining, trim it
  stringstream ss(cmd);
  string command = "";
  int leadingNumber = 0;
  if (isdigit(cmd.at(0))) {
    ss >> leadingNumber;
  }
  ss >> command;

  if (nameShortcutMap_.find(command) != nameShortcutMap_.end()) {
    // found -> valid

    // check if there is a multiplier at front and it's internal command
    if ((leadingNumber != 0) &&
        (find(internalCommandNames.begin(), internalCommandNames.end(),
              nameShortcutMap_[command]) != internalCommandNames.end())) {
      return false;
    }

    // check if there is a multiplier at front & the cmd is the following
    if ((leadingNumber != 0) &&
        (isCmdCommand(command, RESTART) || isCmdCommand(command, HINT) ||
         isCmdCommand(command, NORANDOMFILE) ||
         isCmdCommand(command, RANDOM))) {
      return false;
    }

    return true;
  }
  return false;
}

// First return value is true if the input is a valid command with x parameters
// Second return value is the list of cmd/params parsed from the input
pair<bool, vector<string>> CommandAnalyser::isCmdCommandValidWithXParameters(
    string input, int x) {
  // assume the command is already valid
  // i.e., it has already been tested by isCmdCommandValid()
  //   if (!isCmdCommandValid(input)) {
  //     return false;
  //   }

  stringstream ss(input);
  string cmd = "", parameter = "";
  ss >> cmd;

  bool isValid = false;
  vector<string> paramList = {cmd};
  int counter = x;

  while (ss >> parameter) {
    paramList.push_back(parameter);
    counter--;
  }
  isValid = (counter == 0) ? true : false;

  return make_pair(isValid, paramList);
}

cmdListPair CommandAnalyser::getCommands(string cmd) {
  cmdEnumVec result = cmdEnumVec{};

  // if there exists number in the begining, trim it
  stringstream ss(cmd);
  string command = "";
  int leadingNumber = 0;
  int tmp = 0;
  if (isdigit(cmd.at(0))) {
    ss >> leadingNumber;
  }
  ss >> command;

  if (leadingNumber <= 1) {
    leadingNumber = 0;
  }

  tmp = leadingNumber;

  auto search = nameShortcutMap_.find(command);
  // as long as we call isCmdCommandValid before, we are guaranteed
  // to get a non-empty return value
  if (search != nameShortcutMap_.end()) {
    // a multiplier of 0 means no execution
    if (cmd.at(0) != '0' || tmp != 0) {
      if (tmp >= 1) {
        // exists multiplier
        while (tmp-- >= 1) {
          result.insert(end(result), begin(commandsMap_[search->second]),
                        end(commandsMap_[search->second]));
        }
      } else {
        // no multiplier
        result = commandsMap_[search->second];
      }
    }
  }

  return make_pair(result, size_t(leadingNumber));
}

bool CommandAnalyser::isInternalCommand(string cmd) {
  // if there exists number in the begining, trim it
  stringstream ss(cmd);
  string command = "";
  int leadingNumber = 0;
  if (isdigit(cmd.at(0))) {
    ss >> leadingNumber;
  }
  ss >> command;

  // command in the valid command list
  auto it = nameShortcutMap_.find(command);
  // command is an internal command
  auto isInternal = find(internalCommandNames.begin(),
                         internalCommandNames.end(), (*it).second);

  if (it != nameShortcutMap_.end() &&
      isInternal != internalCommandNames.end()) {
    // found the command AND
    // value is in the internalCommand vector

    string p1 = "", p2 = "", trim = "";
    cmdEnumVec cmds = cmdEnumVec{}, vecTmp = cmdEnumVec{};

    // operating internal command logics
    switch (commandsMap_[(*it).second].at(0)) {
      case RENAME:
        // need exactly two parameters
        ss >> p1 >> p2 >> trim;

        // too few or too many parameters
        if (p2.empty() || !trim.empty()) {
          cout << "Please follow the pattern:\trename "
                  "<odd name> <new name>"
               << endl;
          break;
        }
        // rename the command name from p1 to p2
        if (changeCommandName_(p1, p2)) {
          cout << "Rename completed." << endl;
        }

        break;
      case MACRO:
        // need at least two parameters
        ss >> p1 >> p2;
        // too few parameters
        if (p2.empty()) {
          cout << "Please follow the pattern:\tmacro <command name> "
                  "<command 1> <command 2>[optional]..."
               << endl;
          break;
        }

        // get all the parameters and store them as a cmdEnumVec
        //    Currently changing a command will not affect the commands built
        //    with "macro" command before that.
        //    E.g, "macro cmd left" -> {"cmd":{LEFT}}, now we modify the command
        //    "left" during runtime will not afect the command "cmd" at all.
        // TODO:
        //    Might want to make them related to each other? But if we deleted a
        //    command used in a macro command, then the macro command would not
        //    be able to work anymore.

        if (isCmdCommandValid(p2)) {
          vecTmp = getCommands(p2).first;
          cmds.insert(end(cmds), begin(vecTmp), end(vecTmp));
        } else {
          cout << "[cmdAnalyser]: " << p2 + ": command not found." << endl;
          printBasicCommandInfo();
          break;
        }
        while (!ss.eof()) {
          ss >> p2;
          if (isCmdCommandValid(p2)) {
            vecTmp = getCommands(p2).first;
            cmds.insert(end(cmds), begin(vecTmp), end(vecTmp));
          } else {
            cout << "[cmdAnalyser]: " << p2 + ": command not found." << endl;
            printBasicCommandInfo();
            break;
          }
        }
        // add the command name p1 with the sequence of commands cmds
        if (addNewCommand_(p1, cmds)) {
          cout << "New command added." << endl;
        }

        break;
      case SHOWCMD:
        printValidCmds_("All valid commands:");
        break;
      case HELP:
        printBasicCommandInfo();
      default:
        break;
    }
    return true;
  }
  return false;
}

// Returns true if the string input contains the enum input (command)
bool CommandAnalyser::isCmdCommand(string input, COMMANDENUM cmdEnum) const {
  string shortcut = "", cmd = "";
  stringstream ss(input);
  ss >> shortcut;

  // check shortcuts map for the "alias" of the command
  // as the key in commands map
  auto cmdIt = nameShortcutMap_.find(shortcut);

  // the command shortcut exists
  if (cmdIt != nameShortcutMap_.end()) {
    // get the command "alias"
    cmd = (*cmdIt).second;
    // get the enum of the command and compare
    auto cmdEnumIt = commandsMap_.find(cmd);

    // the command "alias" exists in the commandsMap
    if (cmdEnumIt != commandsMap_.end()) {
      // The basic commands should only has one element in their
      // command lists, and the first one must be the command enum for itself
      if ((*cmdEnumIt).second.at(0) == cmdEnum) {
        return true;
      }
    }
  }
  return false;
}

bool CommandAnalyser::addNewCommand_(string newCmdName, cmdEnumVec cmds) {
  return updateMaps_('a', "", newCmdName, cmds);
}

bool CommandAnalyser::changeCommandName_(string cmd1, string cmd2) {
  if (cmd1 == "help") {
    cout << "command : help cannot be renamed." << endl;
    return false;
  }
  return updateMaps_('c', cmd1, cmd2);
}

bool CommandAnalyser::commandExists_(std::string cmd) const {
  if (find(basicCommandNames.begin(), basicCommandNames.end(), cmd) !=
          basicCommandNames.end() ||
      find(internalCommandNames.begin(), internalCommandNames.end(), cmd) !=
          internalCommandNames.end()) {
    return true;
  }
  return false;
}

// TODO [Optional]:
// could add some descriptions for each command
void CommandAnalyser::printValidCmds_(string info) const {
  if (!info.empty()) {
    cout << info << endl;
  }
  cout << "Indicators: [v] indicates it's allowed, [x] indicates it's not"
       << endl;
  cout << "Basic commands: basic commands used to play the game" << endl;
  for (auto cmd : basicCommandNames) {
    cout << "\t" << cmd << "\t" << getCommandDescription_(cmd) << endl;
  }
  if (!customCommandNames.empty()) {
    cout << "Custom commands: Multiplier [v] (Note: will show the board "
            "multiple times) | Rename [v]"
         << endl;
    for (auto cmd : customCommandNames) {
      cout << "\t" << cmd << endl;
    }
  }
  cout << "Internal commands: commands not related to the game directly"
       << endl;
  for (auto cmd : internalCommandNames) {
    cout << "\t" << cmd << "\t" << getCommandDescription_(cmd) << endl;
  }
}

void CommandAnalyser::printBasicCommandInfo() const {
  cout << "[Help]: Type \"" << internalCommandNames.at(indexForShow)
       << "\" to see all the valid commands." << endl;
}

// get the string for the description of the command
string CommandAnalyser::getCommandDescription_(string cmd) const {
  // the input can be used directly in commandsMap_
  // and the size of the value in the map is exactly 1
  // This is guaranteed by the command Analyser logics (as a whole)

  auto it = commandsMap_.find(cmd);
  // basic check, but really should not happen
  if (it == commandsMap_.end() || (*it).second.empty()) {
    return "";
  }

  string resultStr = "";

  // get the command enum, which is the only thing that is
  // never going to be changed during runtime
  // (the key of the map can be renamed)
  COMMANDENUM cmdEnum = (*it).second.at(0);

  // First, we print the information about allow multiplier or not
  switch (cmdEnum) {
    // Allowed for Multiplier
    case ICOMMAND:
    case TCOMMAND:
    case ZCOMMAND:
    case SCOMMAND:
    case OCOMMAND:
    case LCOMMAND:
    case JCOMMAND:
    case LEFT:
    case RIGHT:
    case DOWN:
    case DROP:
    case LEVELUP:
      resultStr += "\t";
    case LEVELDOWN:
    case CLOCKWISE:
      resultStr += "\t";
    case COUNTERCLOCKWISE:
      resultStr += "Multiplier [v] | ";
      break;
    // Not allowed
    case RENAME:
    case MACRO:
    case SHOWCMD:
    case HELP:
    case RESTART:
    case HINT:
    case RANDOM:
      resultStr += "\t";
    case NORANDOMFILE:
    case SEQUENCEFILE:
      resultStr += "\t";
      resultStr += "Multiplier [x] | ";
      break;
    default:
      break;
  }

  // Indicate if rename is Allowed
  switch (cmdEnum) {
    // Allowed for Multiplier
    case ICOMMAND:
    case TCOMMAND:
    case ZCOMMAND:
    case SCOMMAND:
    case OCOMMAND:
    case LCOMMAND:
    case JCOMMAND:
    case LEFT:
    case RIGHT:
    case DOWN:
    case CLOCKWISE:
    case COUNTERCLOCKWISE:
    case DROP:
    case LEVELUP:
    case LEVELDOWN:
    case RENAME:
    case MACRO:
    case SHOWCMD:
    case RESTART:
    case HINT:
    case NORANDOMFILE:
    case RANDOM:
    case SEQUENCEFILE:
      resultStr += "Rename [v] | ";
      break;
    // Not allowed
    case HELP:
      resultStr += "Rename [x] | ";
      break;
    default:
      break;
  }

  // Indicate the level allowd
  switch (cmdEnum) {
    // Allowed for all levels
    case ICOMMAND:
    case TCOMMAND:
    case ZCOMMAND:
    case SCOMMAND:
    case OCOMMAND:
    case LCOMMAND:
    case JCOMMAND:
    case LEFT:
    case RIGHT:
    case DOWN:
    case CLOCKWISE:
    case COUNTERCLOCKWISE:
    case DROP:
    case LEVELUP:
    case LEVELDOWN:
    case RENAME:
    case MACRO:
    case SHOWCMD:
    case RESTART:
    case HINT:
    case SEQUENCEFILE:
    case HELP:
      resultStr += "All levels [v]";
      break;
    // only 3 and 4
    case NORANDOMFILE:
    case RANDOM:
      resultStr += "Only level 3 and 4 [v]";
      break;
    default:
      break;
  }

  resultStr += "\n\t\t\t\t ";

  // Add descriptions
  switch (cmdEnum) {
    // Change blocks
    case ICOMMAND:
      resultStr += "Changes the current block to I block";
      break;
    case TCOMMAND:
      resultStr += "Changes the current block to T block";
      break;
    case ZCOMMAND:
      resultStr += "Changes the current block to Z block";
      break;
    case SCOMMAND:
      resultStr += "Changes the current block to S block";
      break;
    case OCOMMAND:
      resultStr += "Changes the current block to O block";
      break;
    case LCOMMAND:
      resultStr += "Changes the current block to L block";
      break;
    case JCOMMAND:
      resultStr += "Changes the current block to J block";
      break;
    // block commands
    case LEFT:
      resultStr += "Move the current block left once";
      break;
    case RIGHT:
      resultStr += "Move the current block right once";
      break;
    case DOWN:
      resultStr += "Move the current block down once";
      break;
    case CLOCKWISE:
      resultStr += "Rotate the current block clockwise once";
      break;
    case COUNTERCLOCKWISE:
      resultStr += "Rotate the current block counterclockwise once";
      break;
    case DROP:
      resultStr += "Drop the current block to the bottom";
      break;
    // level commands
    case LEVELUP:
      resultStr += "Level up the current level by one";
      break;
    case LEVELDOWN:
      resultStr += "Level down the current level by one";
      break;
    // game logics
    case RESTART:
      resultStr += "Restart the game";
      break;
    case HINT:
      resultStr += "Provides a hint for current block";
      break;
    case NORANDOMFILE:
      resultStr +=
          "Makes level non-random, takes input from the file indicated";
      resultStr += "\n\t\t\t\t Format: ";
      resultStr += basicCommandNames.at(indexForNoran) + " <file name>";
      break;
    case RANDOM:
      resultStr += "Makes level random (opposed to norandom)";
      break;
    case SEQUENCEFILE:
      resultStr += "Executes the commands from the file indicated";
      resultStr += "\n\t\t\t\t Format: ";
      resultStr += basicCommandNames.at(indexForSeq) + " <file name>";
      break;
    // internal commands
    case RENAME:
      resultStr += "Rename any command that's allowed to be renamed";
      resultStr += "\n\t\t\t\t Format: ";
      resultStr += internalCommandNames.at(indexForRename) +
                   " <old command name> <new command name>";
      break;
    case MACRO:
      resultStr += "Create a custom command";
      resultStr += "\n\t\t\t\t Format: ";
      resultStr += internalCommandNames.at(indexForMacro) +
                   " <command name> <command 1> <command 2> [optional]... ";
      break;
    case SHOWCMD:
      resultStr += "Display all the valid commands (That is, this list)";
      break;
    case HELP:
      resultStr += "Show the [HELP] info (Not this, but a \"link\" to this)";
      break;
    default:
      break;
  }

  return resultStr;
}