// main function for this application

// later use
// #include <gtkmm/main.h>

#include <ctime>
#include <string.h>
#include "gamePlay.h"
#include "controller.h"
#include "gamePlay.h"
#include "graphicalDisplay.h"
#include "textDisplay.h"

using namespace std;

// TODO: Declare flag interpretor

int main(int argc, char* argv[]) {
  //   // Initialize gtkmm with the command line arguments, as appropriate.
  //   Gtk::Main kit(argc, argv);
  //   // Create gamePlay
  //   gamePlay gamePlay;
  //   // Create controller
  //   Controller controller(&gamePlay);
  //   // Create the view -- is passed handle to controller and gamePlay
  //   View view(&controller, &gamePlay);
  //   // Show the window and return when it is closed.
  //   Gtk::Main::run(view);

  // set some default variables
  bool textOnly = false;
  string blockFile = "sequence.txt";
  size_t randomSeed = (unsigned)time(0);
  size_t startLevel = 0;

  // do the argparsing
  for (int i = 0; i < argc; ++i) {
    if (strcmp(argv[i], "-text") == 0) {
      textOnly = true;
    } else if (strcmp(argv[i], "-seed") == 0) {
      randomSeed = stoi(argv[i + 1]);
    } else if (strcmp(argv[i], "-scriptfile") == 0) {
      blockFile = argv[i + 1];
    } else if (strcmp(argv[i], "-startlevel") == 0) {
      startLevel = stoi(argv[i + 1]);
    }
  }

  // passing in 0 to GamePlay so GameLogic can have the current Level
  // LevelUp and LevelDown methods in Controller should also update the
  // "levelType" field in GamePlay
  // the default starting level is not always 0. If -startlevel "n" flag is
  // passed in, "n" is our start level.
  shared_ptr<GamePlay> gp = make_shared<GamePlay>(startLevel);
  shared_ptr<Controller> con =
      make_shared<Controller>(gp, startLevel, randomSeed, blockFile);

  shared_ptr<TextDisplay> textD = make_shared<TextDisplay>(con, gp);
  // shared_ptr<GraphicalDisplay> graphD = make_shared<GraphicalDisplay>(con,
  // gp);

  if (!textOnly) {
    cout << "Let's pretend graphics are running." << endl;
    //(*graphD).run();
  } else {
    cout << "Graphics are NOT running." << endl;
  }
  (*textD).run();
  //(*graphD).run();

  return 0;
}  // main

// TODO: Implement the flag interpretor