// level.h
// Level class. Is responsible for different strategies

#ifndef LEVEL_H
#define LEVEL_H

#include <string>
#include <fstream>
#include "blockenum.h"
#include "commandenum.h"
#include "gamePlay.h"

#define sequenceFile "sequence.txt"

// define Max level
#define MAXLEVEL 4

class Level {
 public:
  Level(size_t levelType);
  ~Level();

  // Calls Model for the following commands
  virtual bool left(std::shared_ptr<GamePlay>);
  virtual bool right(std::shared_ptr<GamePlay>);
  virtual bool down(std::shared_ptr<GamePlay>);
  virtual bool clockwise(std::shared_ptr<GamePlay>);
  virtual bool counterClockwise(std::shared_ptr<GamePlay>);
  virtual bool drop(std::shared_ptr<GamePlay>);
  virtual void restart(std::shared_ptr<GamePlay>);
  virtual void hint(std::shared_ptr<GamePlay>);

  // Set the notify number so that when 5right is called, only displays once
  virtual void setNoNotify(std::shared_ptr<GamePlay>, int, bool);
  // Gets the next block type based on level probabilities
  BLOCKENUM getNextBlock(std::shared_ptr<GamePlay>, size_t seed_);

  // TODO: need to modify isRandom_
  friend class Controller;

 protected:
  // Returns a vector of blockenums corresponding to the block probabilities of
  // each level
  virtual blockEnumVec getBlockProbability(std::shared_ptr<GamePlay>) = 0;

  // Vector of blockenums corresponding to the block probabilities of each level
  blockEnumVec blockPr_;

  // Represents current level
  size_t levelType_;

  // Vector of block enums to feed to the game
  blockEnumVec fileVec_;

  // Vector of block enums for level 0
  blockEnumVec lvlZeroFileVec_;

  // Pointer to elements in fileVec_
  size_t fileVecPtr_;

  // Variable that remembers whether lvl 3 or 4 were random
  bool isRandom_;

 private:
  // Reads a sequence of commands from a file
  void readSequence_(std::string str);
};  // Level

class HeavyBlockLevel : public Level {
 public:
  HeavyBlockLevel(size_t levelType);
  // Overriden because drop is called after command
  bool left(std::shared_ptr<GamePlay>) override;
  bool right(std::shared_ptr<GamePlay>) override;
  bool clockwise(std::shared_ptr<GamePlay>) override;
  bool counterClockwise(std::shared_ptr<GamePlay>) override;
  void setNoNotify(std::shared_ptr<GamePlay>, int, bool) override;
};  // HeavyBlockLevel

class Level0 : public Level {
 public:
  Level0(std::string blockFile,
         std::unordered_map<char, BLOCKENUM> blockChar2Enum);

 protected:
  blockEnumVec getBlockProbability(std::shared_ptr<GamePlay>) override;
};  // Level0

class Level1 : public Level {
 public:
  Level1();

 protected:
  blockEnumVec getBlockProbability(std::shared_ptr<GamePlay>) override;
};  // Level1

class Level2 : public Level {
 public:
  Level2();

 protected:
  blockEnumVec getBlockProbability(std::shared_ptr<GamePlay>) override;
};  // Level2

class Level3 : public HeavyBlockLevel {
 public:
  Level3();

 protected:
  blockEnumVec getBlockProbability(std::shared_ptr<GamePlay>) override;
};  // Level3

class Level4 : public HeavyBlockLevel {
 public:
  Level4();
  ~Level4();

 protected:
  blockEnumVec getBlockProbability(std::shared_ptr<GamePlay>) override;
  // Generates a * block in the middle of the board
  void generateOneBlock(std::shared_ptr<GamePlay>);

 private:
  // Counter for how many blocks are on the board
  int blocknum_;
};  // Level4

#endif