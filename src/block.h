// block.h
// block Class. Is responsible for creating an object for block

#ifndef BLOCKE_H
#define BLOCKE_H

#include <map>
#include <memory>
#include <set>
#include "blockenum.h"

// Anchor coordinate of the block - bottom left of "box" the block fits in
typedef std::vector<int> anchor;
// Set of coordinates (relative to anchor) of all blocks in the structure
typedef std::set<anchor> coordinates;
// Vector of all rotation configurations of a block
typedef std::vector<coordinates> rotations;

class Block {
 public:
  Block(BLOCKENUM, int);
  ~Block();

  // Accessors
  // Returns current coords
  coordinates getCoordinates() const;
  // Returns block type
  BLOCKENUM getType() const;

  // Accessor
  // Returns how many blocks from this block are remaining on the board
  int getRemainingBlocks() const;
  // Returns the level this block was generated in
  int getLevelOfOrigin() const;

  // Translation and rotation testers
  // Returns coordinates for if the block were to move
  coordinates getLeftCoordinates() const;
  coordinates getRightCoordinates() const;
  coordinates getDownCoordinates() const;
  coordinates getClockwiseCoordinates() const;
  coordinates getCounterClockwiseCoordinates() const;

  // get the string to print the block
  // but it adds the input before each line
  std::string getStr4Block(std::string) const;

  // Translation and rotation setters
  void left();
  void right();
  void down();
  void clockwise();
  void counterClockwise();

  // Mutators
  // Mutator for remaining blocks
  void setRemainingBlocks(const int remainingBlocks);

  // Overloaded Operators
  friend std::ostream &operator<<(std::ostream &, Block &);

 private:
  // helper function for getting test coords
  coordinates getTestCoords(anchor, coordinates) const;

  // The type of the block
  BLOCKENUM blockType_;
  // should have exactly 2 elements, as a coordinate
  anchor anchor_;
  // coordinates of all rotation configurations of the block
  rotations rotConfig_;
  // equal to one of one of the rotations configurations
  coordinates relativeCoords_;
  int rotConfigPosition_;
  // number of squares of the block still left (for scoring purposes - see
  // "Bonus Scoring" section of Specs)
  int remainingBlocks_;
  // level the block was generated at
  int levelOfOrigin_;
};  // Block

#endif