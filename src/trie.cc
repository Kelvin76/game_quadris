// Implementing trie.h
// Trie class. A trie class for lower-case english characters.

#include <cstring>
#include "trie.h"

using namespace std;

Trie::Trie() {
  isEnd = false;
  memset(Next, 0, sizeof(Next));
}

void Trie::insert(string word) {
  shared_ptr<Trie> curNode = make_shared<Trie>(*this);
  for (char chr : word) {
    int idx = chr - 'a';
    if (idx < 0 || idx >= totalNumOfChar) {
      return;
    }
    if (curNode->Next[idx] == nullptr) {
      curNode->Next[idx] = make_shared<Trie>();
    }
    curNode = curNode->Next[idx];
  }
  curNode->isEnd = true;
}

bool Trie::search(string word) {
  shared_ptr<Trie> curNode = make_shared<Trie>(*this);
  for (char chr : word) {
    int idx = chr - 'a';
    if (idx < 0 || idx >= totalNumOfChar) {
      return false;
    }
    if (curNode->Next[idx] == nullptr) {
      return false;
    }
    curNode = curNode->Next[idx];
  }
  return curNode->isEnd;
}

bool Trie::startsWith(string prefix) {
  shared_ptr<Trie> curNode = make_shared<Trie>(*this);
  for (char chr : prefix) {
    int idx = chr - 'a';
    if (idx < 0 || idx >= totalNumOfChar) {
      return false;
    }
    if (curNode->Next[idx] == nullptr) {
      return false;
    }
    curNode = curNode->Next[idx];
  }
  return true;
}

bool Trie::multipleStartsWith(string prefix) {
  shared_ptr<Trie> curNode = make_shared<Trie>(*this);
  for (char chr : prefix) {
    int idx = chr - 'a';
    if (idx < 0 || idx >= totalNumOfChar) {
      return false;
    }
    if (curNode->Next[idx] == nullptr) {
      return false;
    }
    curNode = curNode->Next[idx];
  }
  if (curNode->hasTwoChildren_()) {
    return true;
  }
  return false;
}

string Trie::findShortcut(string str) {
  // assume the str is absolutely in the trie
  // this can be achieved
  shared_ptr<Trie> curNode = make_shared<Trie>(*this);
  string prefix = "";
  for (char chr : str) {
    int idx = chr - 'a';
    if (curNode->hasTwoChildren_()) {
      prefix.push_back(chr);
      curNode = curNode->Next[idx];
    } else {
      break;
    }
  }
  return prefix;
}

std::vector<std::string> Trie::findAllShortcut(string str) {
  string shortcut = findShortcut(str);
  std::vector<std::string> result;

  result.push_back(shortcut);
  str = str.substr(shortcut.size());

  for (auto chr : str) {
    shortcut.push_back(chr);
    result.push_back(shortcut);
  }

  return result;
}

bool Trie::hasTwoChildren_() {
  bool found = isEnd;
  shared_ptr<Trie> tmp = make_shared<Trie>();
  shared_ptr<Trie> curNode = make_shared<Trie>(*this);
  for (int idx = 0; idx < totalNumOfChar; ++idx) {
    if (curNode->Next[idx] != nullptr) {
      if (found == true) {
        return true;
      }
      tmp = curNode->Next[idx];
      found = true;
    }
  }
  if (found == false || tmp == nullptr) {
    return false;
  }
  return tmp->hasTwoChildren_();
}