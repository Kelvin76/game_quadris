// display.h
// Display class.  Is responsible for both graphic and text interface

#ifndef MVC_Display_H
#define MVC_Display_H

// later use
// #include <gtkmm.h>

#include <memory>
#include "commandAnalyser.h"
#include "controller.h"
#include "gamePlay.h"
#include "observer.h"

class Controller;
class GamePlay;

class Display : public Observer {
public:
    Display(std::shared_ptr<Controller>, std::shared_ptr<GamePlay>);
    virtual ~Display();

    // Updates the fields, then calls display
    virtual void update() override;
    // Displays the board
    virtual void display() = 0;
    // Run/start the view, used by the main function
    virtual void run() = 0;

protected:
    // Subject
    std::shared_ptr<GamePlay> model_;
    // Strategy Pattern member (plus signal handlers)
    std::shared_ptr<Controller> controller;
    // Command analyser pointer for the input
    std::shared_ptr<CommandAnalyser> cmdAnalyser;

    // Type of the nextBlock
    BLOCKENUM nextBlock;
    // Pointer to the board
    std::shared_ptr<Board> board;
    // Current level
    size_t level;
    // Current score
    size_t score;
    // High score
    size_t hiScore;
    // If game in play, is true
    bool gameInPlay;

    // Member widgets:
    // Gtk::HBox panels;  // Main window divided into two horizontal panels
    // Gtk::VBox butBox;  // Vertical boxes for stacking buttons vertically
    // Gtk::Button next_button;
    // Gtk::Button reset_button;
    // Gtk::Image card;

    // Signal handlers:
    // void nextButtonClicked();
    // void resetButtonClicked();

};  // Display

#endif