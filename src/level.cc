// Implementing level.h
// Level class(es). Is(Are) responsible for different strategies

#include <ctime>
#include <fstream>
#include <random>
#include "level.h"

using namespace std;

// ------ Level ------
Level::Level(size_t levelType)
    : levelType_(levelType), fileVecPtr_(0), isRandom_(true) {}

Level::~Level() {}

bool Level::left(shared_ptr<GamePlay> g) { return g->left(); }

bool Level::right(shared_ptr<GamePlay> g) { return g->right(); }

bool Level::down(shared_ptr<GamePlay> g) { return g->down(); }

bool Level::clockwise(shared_ptr<GamePlay> g) { return g->clockwise(); }

bool Level::counterClockwise(shared_ptr<GamePlay> g) {
  return g->counterClockwise();
}

bool Level::drop(shared_ptr<GamePlay> g) { return g->drop(); }

void Level::restart(shared_ptr<GamePlay> g) { g->restart(); }

void Level::hint(shared_ptr<GamePlay> g) { g->hint(); }

BLOCKENUM Level::getNextBlock(shared_ptr<GamePlay> g, size_t randomSeed) {
  // cout << "Level: getNextBlock" << endl; //TODO: Take out print
  if ((levelType_ == 1 || levelType_ == 2) ||
      ((levelType_ == 3 || levelType_ == 4) && isRandom_ == true)) {
    // generate a "random" seed
    static mt19937 rng(randomSeed);
    // get the probability vector
    blockEnumVec blockProbabilities = getBlockProbability(g);
    // get a random valid index
    int size = blockProbabilities.size();
    int index = (int)(rng() % size);
    // return the block type at that index
    return blockProbabilities.at(index);
  } else if (levelType_ == 0) {
    BLOCKENUM b = fileVec_[fileVecPtr_];
    if (fileVecPtr_ < fileVec_.size() - 1) {
      // if EOF, go back to beginning of file
      fileVecPtr_ += 1;
    } else {
      fileVecPtr_ = 0;
    }
    return b;
  } else {
    // if it's lvl 3 and 4 and isRandom is true
    BLOCKENUM b = lvlZeroFileVec_[fileVecPtr_];
    if (fileVecPtr_ < lvlZeroFileVec_.size() - 1) {
      // if EOF, go back to beginning of file
      fileVecPtr_ += 1;
    } else {
      fileVecPtr_ = 0;
    }
    return b;
  }
}

void Level::setNoNotify(std::shared_ptr<GamePlay> g, int counter, bool isDown) {
  g->setNoNotify(counter);
}

void Level::readSequence_(std::string str) {}

// ------ HeavyBlockLevel ------
HeavyBlockLevel::HeavyBlockLevel(size_t levelType) : Level(levelType) {}

bool HeavyBlockLevel::left(shared_ptr<GamePlay> g) {
  bool moveSuccessful = g->left();
  //  if (moveSuccessful){
  //    g->down();
  //  }
  g->down();
  return moveSuccessful;
}

bool HeavyBlockLevel::right(shared_ptr<GamePlay> g) {
  bool moveSuccessful = g->right();
  //  if (moveSuccessful){
  //    g->down();
  //  }
  g->down();
  return moveSuccessful;
}

bool HeavyBlockLevel::clockwise(shared_ptr<GamePlay> g) {
  bool moveSuccessful = g->clockwise();
  //  if (moveSuccessful){
  //    g->down();
  //  }
  g->down();
  return moveSuccessful;
}

bool HeavyBlockLevel::counterClockwise(shared_ptr<GamePlay> g) {
  bool moveSuccessful = g->counterClockwise();
  //  if (moveSuccessful){
  //    g->down();
  //  }
  g->down();
  return moveSuccessful;
}

void HeavyBlockLevel::setNoNotify(std::shared_ptr<GamePlay> g, int counter,
                                  bool isDown) {
  // down command does not need to execute optional down commands
  if (isDown) {
    g->setNoNotify(counter);
  } else {
    g->setNoNotify(counter * 2);
  }
}

// ------------ Levels ------------
Level0::Level0(string blockFile, unordered_map<char, BLOCKENUM> blockChar2Enum)
    : Level(0) {
  char input = (char)0;
  ifstream file;
  file.open(blockFile);

  // file does not exist
  if (!file) {
    cout << "[Error]: File indicated does not exist, please enter again."
         << endl;
    return;
  }

  // create the rest of the vec
  blockEnumVec bv;
  while (file >> input) {
    bv.push_back(blockChar2Enum[input]);
  }

  fileVec_ = bv;
}

blockEnumVec Level0::getBlockProbability(shared_ptr<GamePlay> g) {
  return blockEnumVec{};
}

Level1::Level1() : Level(1) {}

blockEnumVec Level1::getBlockProbability(shared_ptr<GamePlay> g) {
  return blockEnumVec{ZBLOCK, SBLOCK, IBLOCK, IBLOCK, TBLOCK, TBLOCK,
                      OBLOCK, OBLOCK, LBLOCK, LBLOCK, JBLOCK, JBLOCK};
}

Level2::Level2() : Level(2) {}

blockEnumVec Level2::getBlockProbability(shared_ptr<GamePlay> g) {
  return blockEnumVec{ZBLOCK, SBLOCK, IBLOCK, TBLOCK, OBLOCK, LBLOCK, JBLOCK};
}

Level3::Level3() : HeavyBlockLevel(3) {}

blockEnumVec Level3::getBlockProbability(shared_ptr<GamePlay> g) {
  return blockEnumVec{ZBLOCK, ZBLOCK, SBLOCK, SBLOCK, IBLOCK,
                      TBLOCK, OBLOCK, LBLOCK, JBLOCK};
}

Level4::Level4() : HeavyBlockLevel(4), blocknum_{0} {};

Level4::~Level4(){};

blockEnumVec Level4::getBlockProbability(shared_ptr<GamePlay> g) {
  blocknum_++;
  if (blocknum_ % 5 == 0) generateOneBlock(g);
  return blockEnumVec{ZBLOCK, ZBLOCK, SBLOCK, SBLOCK, IBLOCK,
                      TBLOCK, OBLOCK, LBLOCK, JBLOCK};
}

void Level4::generateOneBlock(std::shared_ptr<GamePlay> g) {
  g->generateOneBlock();
}