// Implementing textDisplay.h
// TextDisplay class.  Is responsible for text interface

#include <fstream>
#include <iostream>
#include "textDisplay.h"

using namespace std;

TextDisplay::TextDisplay(std::shared_ptr<Controller> c,
                         std::shared_ptr<GamePlay> m)
    : Display(c, m) {
  // Register TextDisplay as observer of gamePlay
  subject_->subscribe(this);
}

TextDisplay::~TextDisplay() {
  subject_->unsubscribe(this);
}

void TextDisplay::display() {
  if (gameInPlay) {
    // number of spaces to print before every print
    string spaces = getXSpaces((*board).getRowDigit());

    // diplay is 13 + (number of row digits) characters across
    // the board itself is 11 characters across
    cout << spaces << "Level:      " << level << endl;
    cout << spaces << "Score:      " << score << endl;
    cout << spaces << "Hi Score:   " << hiScore << endl;

    // print upper separator
    cout << spaces;
    cout << "-----------" << endl;

    // prints board
    cout << (*board) << endl;

    // print bottom separator
    cout << spaces;
    cout << "-----------" << endl;

    // print the Next block
    cout << spaces;
    cout << "Next:" << endl;
    unique_ptr<Block> bl =
        make_unique<Block>(nextBlock, controller->getLevelType());
    cout << (*bl).getStr4Block(spaces) << endl;
  } else {
    cout << "Board is full, Game Over! :( Starting new round." << endl;
  }
}

void TextDisplay::run() {
  // show the view
  display();

  // print additional info
  (*cmdAnalyser).printBasicCommandInfo();

  // repeatedly parsing the input until EOF
  string input = "";
  while (getline(cin, input)) {
    if (cmdAnalyser->isCmdCommandValid(input)) {
      // execute the sequence of commands in controller,
      // if the command is valid

      // do nothing if the command is an internal command
      if (cmdAnalyser->isInternalCommand(input)) {
        continue;
      }

      controller->commandInterpreter(cmdAnalyser, input);
    } else {
      // command is empty, does nothing
      if (!input.empty()) {
        cout << "[Controller]: " << input
             << ": command not found, please enter again:" << endl;
      }
    }
  }
}