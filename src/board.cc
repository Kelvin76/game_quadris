// Implementing board.h
// Board class. Is responsible for a functional board class

#include "board.h"

using namespace std;

Board::Board(size_t row, size_t col) : rowNum_(row), colNum_(col) {
  // resize the number of rows
  board_.resize(rowNum_);

  // calculate how many digits the rowNum has
  int tmp = rowNum_;
  numRowDigit_ = 1;
  while (tmp /= 10) {
    numRowDigit_++;
  }

  // resize all rows to have 11 columns
  for (size_t i = 0; i < rowNum_; i++) {
    for (size_t j = 0; j < colNum_; j++) {
      // creates a new cell object with a space inside and puts that
      // into the board if don't know if pointers are needed here but
      // it won't take too long to change it if need be
      Cell c = Cell(' ');
      board_[i].push_back(c);
    }
  }
}

Board::~Board() {}

bool Board::newBlock(shared_ptr<Block> b, bool override,
                     coordinates oldcoords) {
  coordinates newcoords = (*b).getCoordinates();

  if (override) {
    // if we are overriding the current block then we want to erase the
    // current block
    for (auto coord : oldcoords) {
      board_.at(coord.at(0)).at(coord.at(1)).setChar(' ');
      // If it does, pass in BlockEnum to Cell's "setBlock function"
      // and create a unique pointer there
      board_.at(coord.at(0)).at(coord.at(1)).setBlock(nullptr);
    }
  }
  // check that this block can fit in the desired location
  for (auto coord : newcoords) {
    if (board_.at(coord.at(0)).at(coord.at(1)).getChar() != ' ') {
      return false;
    }
  }

  // it can fit, so then add the block to the board
  for (auto coord : newcoords) {
    // sets to new char and pointer to block
    board_.at(coord.at(0)).at(coord.at(1)).setChar((char)(*b).getType());
    board_.at(coord.at(0)).at(coord.at(1)).setBlock(b);
  }
  return true;
}

bool Board::addOneBlock() {
  // create the block
  shared_ptr<Block> oneBlock = make_shared<Block>(BLOCKENUM::ONEBLOCK, 4);
  coordinates coords = oneBlock->getCoordinates();

  // check that this block can fit in the desired location
  for (auto coord : coords) {
    if (board_.at(coord.at(0)).at(coord.at(1)).getChar() != ' ') {
      return false;
    }
  }

  // add the block to the board
  for (auto coord : coords) {
    board_.at(coord.at(0)).at(coord.at(1)).setChar((char)(*oneBlock).getType());
    board_.at(coord.at(0)).at(coord.at(1)).setBlock(oneBlock);
  }

  // drop the block
  coordinates possiblecoords = oneBlock->getDownCoordinates();
  bool movePossible = move(possiblecoords, coords);

  while (movePossible) {
    oneBlock->down();

    coords = oneBlock->getCoordinates();
    possiblecoords = oneBlock->getDownCoordinates();
    movePossible = move(possiblecoords, coords);
  }
  return true;
}

bool Board::move(coordinates possiblecoords, coordinates coords) {
  // check that this block can fit in the desired location
  for (auto coord : possiblecoords) {
    if (((coord.at(0) < 0) || (coord.at(0) >= int(rowNum_))) ||
        ((coord.at(1) < 0) || (coord.at(1) >= int(colNum_))) ||
        ((coords.find(coord) == coords.end()) &&
         (board_.at(coord.at(0)).at(coord.at(1)).getChar() != ' '))) {
      return false;
    }
  }

  auto first = (*coords.begin());

  char blockChar = board_.at(first.at(0)).at(first.at(1)).getChar();

  shared_ptr<Block> blockObj =
      board_.at(first.at(0)).at(first.at(1)).getBlock();

  for (auto coord : coords) {
    // set new char and new block to cell class
    board_.at(coord.at(0)).at(coord.at(1)).setChar(' ');
    board_.at(coord.at(0)).at(coord.at(1)).setBlock(nullptr);
  }

  for (auto coord : possiblecoords) {
    board_.at(coord.at(0)).at(coord.at(1)).setChar(blockChar);
    board_.at(coord.at(0)).at(coord.at(1)).setBlock(blockObj);
  }

  return true;
}

void Board::hint() {}

void Board::reset() {
  // clear the board
  for (auto rows : board_) {
    rows.clear();
  }
  board_.clear();

  // resize the number of rows
  board_.resize(rowNum_);

  // resize all rows to have 11 columns
  for (size_t i = 0; i < rowNum_; i++) {
    for (size_t j = 0; j < colNum_; j++) {
      // creates a new cell object with a space inside and puts that into
      // the board if don't know if pointers are needed here but it won't
      // take too long to change it if need be
      Cell c = Cell(' ');
      board_[i].push_back(c);
    }
  }
}

// Accessors
size_t Board::getRowNum() const { return rowNum_; }

size_t Board::getColNum() const { return colNum_; }

size_t Board::getRowDigit() const { return numRowDigit_; }

vector<vector<Cell>> Board::getGrid() const { return board_; }

// Mutator
void Board::setGrid(const vector<vector<Cell>> board) { board_ = board; }

// Friended ostream operator
ostream &operator<<(std::ostream &sout, Board &b) {
  // know the maximum of digit for row indicator
  size_t tmp = 0, curDigit = 1;

  // print board
  for (size_t i = 0; i < b.rowNum_; i++) {
    // print the row indicator
    tmp = i + 1;
    curDigit = 1;
    while (tmp /= 10) {
      curDigit++;
    }

    sout << (i + 1);
    // print blank to fill in between the row and indicator
    sout << getXSpaces(b.numRowDigit_ - curDigit);

    // print the row
    for (size_t j = 0; j < b.colNum_; j++) {
      sout << b.board_[i][j].getChar();
    }
    if (i != b.rowNum_ - 1) {
      sout << endl;
    }
  }

  return sout;
}

// helper function
string getXSpaces(int x) {
  string result = "";
  while (x--) {
    result += " ";
  }
  return result;
};