// blockenum.h
// BLOCKENUM. Is responsible for enumeration of all valid blocks

#ifndef BLOCKENUM_H
#define BLOCKENUM_H

#include <unordered_map>
#include <vector>

enum BLOCKENUM {
    IBLOCK = 'I',
    TBLOCK = 'T',
    ZBLOCK = 'Z',
    SBLOCK = 'S',
    OBLOCK = 'O',
    LBLOCK = 'L',
    JBLOCK = 'J',
    ONEBLOCK = '*'
};

typedef std::vector<BLOCKENUM> blockEnumVec;

#endif