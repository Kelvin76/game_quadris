// subject.h
// Subject class.  Abstract class for Observer Pattern

#ifndef MVC_SUBJECT_H
#define MVC_SUBJECT_H

#include <memory>
#include <set>

class Observer;

typedef std::set<Observer*> Observers;

class Subject {
public:
    // Adds the Observer to list of subscribers
    void subscribe(Observer*);
    // Removes the Observer from list of subscribers
    void unsubscribe(Observer*);

protected:
    // Notifies each of the subscribed observers
    void notify();

private:
    // Set of subscribbed Observers
    Observers observers_;
};  // Subject

#endif