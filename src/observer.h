// observer.h
// Observer class.  Abstract class for Observer Pattern

#ifndef OBSERVER_H
#define OBSERVER_H

#include <memory>

class GamePlay;

class Observer {
 public:
  Observer(std::shared_ptr<GamePlay>);
  virtual ~Observer();
  virtual void update() = 0;

 protected:
  std::shared_ptr<GamePlay> subject_;
};  // Observer

#endif