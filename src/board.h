// board.h
// Board class. Is responsible for a functional board class

#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include "cell.h"

#define ROWSAVE 3
#define ROWNUM 15
#define COLNUM 11

class Block;
class Cell;

class Board {
 public:
  Board(size_t = ROWNUM + ROWSAVE, size_t = COLNUM);
  ~Board();

  // Adds the given block to the board
  bool newBlock(std::shared_ptr<Block>, bool, coordinates);
  // Adds a * block to the centre of the board (Level 4)
  bool addOneBlock();
  // Determines if a move to the new location is possible
  bool move(coordinates, coordinates);
  // Determines the optimal location to place the current block
  void hint();
  // Clears the board
  void reset();

  // Accessors
  size_t getRowNum() const;
  size_t getColNum() const;
  size_t getRowDigit() const;
  std::vector<std::vector<Cell>> getGrid() const;

  // Setters
  void setGrid(const std::vector<std::vector<Cell>> board);

  friend std::ostream &operator<<(std::ostream &, Board &);

 private:
  size_t rowNum_;
  size_t colNum_;
  size_t numRowDigit_;
  std::vector<std::vector<Cell>> board_;
};  // Board

std::string getXSpaces(int x);

#endif